#!/usr/bin/python -u
msg_old = r"""<?xml version="1.0" ?>
<testsuite errors="1" failures="5" name="nose2-junit" skips="1" tests="25" time="0.004">
  <testcase classname="pkg1.test.test_things" name="test_gen:1" time="0.000141" />
  <testcase classname="pkg1.test.test_things" name="test_gen:2" time="0.000093" />
  <testcase classname="pkg1.test.test_things" name="test_gen:3" time="0.000086" />
  <testcase classname="pkg1.test.test_things" name="test_gen:4" time="0.000086" />
  <testcase classname="pkg1.test.test_things" name="test_gen:5" time="0.000087" />
  <testcase classname="pkg1.test.test_things" name="test_gen_nose_style:1" time="0.000085" />
  <testcase classname="pkg1.test.test_things" name="test_gen_nose_style:2" time="0.000090" />
  <testcase classname="pkg1.test.test_things" name="test_gen_nose_style:3" time="0.000085" />
  <testcase classname="pkg1.test.test_things" name="test_gen_nose_style:4" time="0.000087" />
  <testcase classname="pkg1.test.test_things" name="test_gen_nose_style:5" time="0.000086" />
  <testcase classname="pkg1.test.test_things" name="test_params_func:1" time="0.000093" />
  <testcase classname="pkg1.test.test_things" name="test_params_func:2" time="0.000098">
    <failure message="test failure">Traceback (most recent call last):
  File "nose2/plugins/loader/parameters.py", line 162, in func
    return obj(*argSet)
  File "nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py", line 64, in test_params_func
    assert a == 1
AssertionError
</failure>
  </testcase>
  <testcase classname="pkg1.test.test_things" name="test_params_func_multi_arg:1" time="0.000094" />
  <testcase classname="pkg1.test.test_things" name="test_params_func_multi_arg:2" time="0.000089">
    <failure message="test failure">Traceback (most recent call last):
  File "nose2/plugins/loader/parameters.py", line 162, in func
    return obj(*argSet)
  File "nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py", line 69, in test_params_func_multi_arg
    assert a == b
AssertionError
</failure>
  </testcase>
  <testcase classname="pkg1.test.test_things" name="test_params_func_multi_arg:3" time="0.000096" />
  <testcase classname="" name="test_fixt" time="0.000091" />
  <testcase classname="" name="test_func" time="0.000084" />
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_failed" time="0.000113">
    <failure message="test failure">Traceback (most recent call last):
  File "nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py", line 17, in test_failed
    assert False, "I failed"
AssertionError: I failed
</failure>
  </testcase>
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_ok" time="0.000093" />
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_params_method:1" time="0.000099" />
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_params_method:2" time="0.000101">
    <failure message="test failure">Traceback (most recent call last):
  File "nose2/plugins/loader/parameters.py", line 144, in _method
    return method(self, *argSet)
  File "nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py", line 29, in test_params_method
    self.assertEqual(a, 1)
AssertionError: 2 != 1
</failure>
  </testcase>
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_skippy" time="0.000104">
    <skipped />
  </testcase>
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_typeerr" time="0.000096">
    <error message="test failure">Traceback (most recent call last):
  File "nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py", line 13, in test_typeerr
    raise TypeError("oops")
TypeError: oops
</error>
  </testcase>
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_gen_method:1" time="0.000094" />
  <testcase classname="pkg1.test.test_things.SomeTests" name="test_gen_method:2" time="0.000090">
    <failure message="test failure">Traceback (most recent call last):
  File "nose2/plugins/loader/generators.py", line 145, in method
    return func(*args)
  File "nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py", line 24, in check
    assert x == 1
AssertionError
</failure>
  </testcase>
</testsuite>"""

msg = r"""
<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
   <testsuite name="Map Access Suite">
      <testsuite name="MapAccess Transit Suite">
         <testcase name="Verify that Powell St. BART Station exists in the vector tiles and matches ID: 5997867451158533287." time="0.05">
            <failure message="test failure">Powell St. BART Station not found in vector tile.</failure>
         </testcase>
         <testcase name="Verify that Grand Central Station exists in the vector tiles and matches ID: 5022784082689922593." time="0.75">
            <failure message="test failure">Grand Central Station found but has unexpected ID. Expected: 5022784082689922593 | Found: 5427743003298079056</failure>
         </testcase>
         <testcase name="Verify that Millbrae Caltrain Station exists in the vector tiles and matches ID: 15642797764623974066." time="0.58">
            <failure message="test failure">Millbrae Caltrain Station found but has unexpected ID. Expected: 15642797764623974066 | Found: 4481701027853121182</failure>
         </testcase>
      </testsuite>
      <testsuite name="MapAccess Drive Suite">
         <testcase name="Verify that San Francisco International Airport BART Station exists in the vector tiles and matches ID: 18054191999158899241." time="0.6">
            <failure message="test failure">San Francisco International Airport BART Station not found in vector tile.</failure>
         </testcase>
         <testcase name="Verify MUNI line from Sunset Tunnel East Portal to Duboce &amp; Church exists in vector tiles with matching IDs." time="0.08">
            <failure message="test failure" />
         </testcase>
         <testcase name="Verify that Embarcadero BART Station exists in the vector tiles and matches ID: 5355592503671394953." time="0.0012">
            <failure message="test failure">Embarcadero BART Station found but has unexpected ID. Expected: 5355592503671394953 | Found: 13582049920169756765</failure>
         </testcase>
         <testcase name="Verify Caltrain line from 22nd St. to San Francisco Caltrain exists in vector tiles with matching IDs." time="0.01234">
            <failure message="test failure">Caltrain line from 22nd St. to San Francisco Caltrain not found in vector tiles.</failure>
         </testcase>
      </testsuite>
   </testsuite>
   <testsuite name="Canonical Suite">
      <testcase name="canonicalSearchTest1" />
      <testcase name="canonicalSearchForceFailureTest1" />
   </testsuite>
   <testsuite name="Stockholm Suite">
      <testcase name="Stockholm Search Test1: Merchant=OFFICE DEPOT #2358, Payment=MasterCard, Location=(37.340400, -121.908000)" time="0.83" />
      <testcase name="Stockholm Search ForceFailure Test1: Merchant=OFFICE DEPOT #2358, Payment=MasterCard, Location=(0.000000, 0.000000)">
         <failure message="test failure">GEOError: No Results</failure>
      </testcase>
      <testcase name="Stockholm Search Test2: Merchant=OFFICE DEPOT #2358, Payment=American Express, Location=(37.340400, -121.908000)" time="8.94">
         <failure>Search for metchant:OFFICE DEPOT #2358 with payment:American Express and GEOLocation(37.340400, -121.908000) should return nil, but returned an array with 1 items</failure>
      </testcase>
   </testsuite>
</testsuites>
"""

code = """spawn ssh zchen@17.228.136.182 "python ~/src/test.py"
expect "Password:"
send -- "32167\r""
send -- "exit\r"
expect eof
puts $expect_out(buffer)
"""

import subprocess
if __name__ == '__main__':
    ###########################################################################
    # proc = subprocess.Popen(['expect', '-c', code], stdout=subprocess.PIPE) #
    # stdout_value = proc.communicate()                                       #
    # xmlStr = ''.join(stdout_value[0].split('\n')[3:-2])                     #
    # with open('output.xml', 'w') as f:                                      #
    #     f.write(xmlStr)                                                     #
    ###########################################################################
    with open('output.txt', 'w') as f:
        f.write(msg)
